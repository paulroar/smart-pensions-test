#!/usr/bin/env ruby
# an exercise in how a solution to this trivial test can be boiled down to just a few lines of ruby
require 'sqlite3'

db = SQLite3::Database.open ':memory:'
db.results_as_hash = true
db.execute('create table weblog(page text, ip_address text)')

File.foreach(ARGV[0]) { |line|
    entry = line.chomp!.split
    next if(not entry.length == 2)
    db.execute 'insert into weblog(page,ip_address) values(?,?)', entry[0], entry[1]
}

puts('Total page views')
puts('-------------------')

db.query('select page, count(1) as hits 
                                   from (select page,ip_address from weblog  group by page,ip_address) unique_visits 
                                   group by page order by count(1) desc').each { |result|
	puts(result['page'] + ' ' + result['hits'].to_s)
}
puts
puts('Unique page views')
puts('-------------------')
db.query('select page, count(1) as hits 
                                   from (select page,ip_address from weblog  group by page,ip_address) unique_visits 
                                   group by page order by count(1) desc').each { |result|
	puts(result['page'] + ' ' + result['hits'].to_s)
}