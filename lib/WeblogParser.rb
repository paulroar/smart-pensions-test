require 'sqlite3'

class WeblogParser
    attr_accessor :logfile

    def initialize
        @db = nil
    end
    
    def pages_ordered_by_total_hits
        if(not @db)
            load_file
        end
        query_results = @db.query('select page, count(1) as hits from weblog group by page order by count(1) desc')
        results = []
        query_results.each { |row| 
            results.append({:hits => row['hits'], :page => row['page']})
        }
        return results
    end 

    def pages_ordered_by_unique_hits
        if(not @db)
            load_file
        end
        query_results = @db.query('select page, count(1) as hits 
                                   from (select page,ip_address from weblog  group by page,ip_address) unique_visits 
                                   group by page order by count(1) desc')
        results = []
        query_results.each { |row| 
            results.append({:hits => row['hits'], :page => row['page']})
        }
        return results
    end

    private 

    def load_file
        @db = SQLite3::Database.open ':memory:'
        @db.results_as_hash = true
        @db.execute('create table if not exists weblog(page text, ip_address text)')
        @db.execute('delete from weblog')

        File.foreach(@logfile) { |line|
            line = line.chomp!
            entry = line.split
            if(not entry.length == 2)
                next
            end
            @db.execute 'insert into weblog(page,ip_address) values(?,?)', entry[0], entry[1]
        }
    end
end