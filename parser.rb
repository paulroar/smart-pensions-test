#!/usr/bin/env ruby
$LOAD_PATH << './lib'
require 'WeblogParser'

logfile = ARGV[0]

if(not logfile)
	abort('Logfile required')
end

if(not File.file?(logfile))
	abort('Logfile does not exist')
end

parser = WeblogParser.new
parser.logfile = logfile

puts('Total page views')
puts('-------------------')

results = parser.pages_ordered_by_total_hits
results.each { |result|
	puts(result[:page] + ' ' + result[:hits].to_s)
}

puts
puts('Unique page views')
puts('-------------------')

results = parser.pages_ordered_by_unique_hits
results.each { |result|
	puts(result[:page] + ' ' + result[:hits].to_s)
}

