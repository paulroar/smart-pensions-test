require 'WeblogParser'

RSpec.describe WeblogParser, '#analyse' do
    parser = WeblogParser.new
    parser.logfile = __dir__ + '/../test-logfiles/test.log'
    it "Returns pages ordered by total hits, most first" do
        expect(parser.pages_ordered_by_total_hits).to eq [ { :page => '/page/one', :hits => 4 },
                                                            { :page => '/page/two', :hits => 2 },
                                                            { :page => '/page/three', :hits => 1 } ]
    end
    it "Returns pages ordered by unique hits, most first" do
        expect(parser.pages_ordered_by_unique_hits).to eq [ { :page => '/page/one', :hits => 3 },
                                                            { :page => '/page/two', :hits => 2 },
                                                            { :page => '/page/three', :hits => 1 } ]
    end     
end
